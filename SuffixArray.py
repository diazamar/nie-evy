import sys


def lcp(str1,str2):
    n1 = len(str1)
    n2 = len(str2)
    leng=min(n1,n2)
     
    result = ""    
    # Compare str1 and str2
    j = 0
    i = 0
    while(i <= leng-1):
        if (str1[i] != str2[j]):
            break
        result += (str1[i])
         
        i += 1
        
 
    return (len(result))

def SuffixArray(str):

    return sorted(range(len(str)), key=lambda i: str[i:]) 
    #since str[i:] is obtaining the suffix for every possition, we use the sorted function to obtain the lex. order

def SimpleSearch(L,x):
    m=len(x)
    n=len(L)-1
    d=-1
    f=n
    while d+1<f:
        i=(d+f)//2
        print(str(d)+" "+str(f)+" "+str(i)+" ")

        ele=lcp(L[i],x)
        if ele==m and ele==len(L[i]):
            return i, None
            break
        elif ele==len(L[i]) or (ele!=m and L[i][ele]<x[ele]):
            d=i
            print(str(d)+" "+str(f)+" "+str(i)+" ")
        else:
            f=i 
            print(str(d)+" "+str(f)+" "+str(i)+" ")
    print(str(d)+" "+str(f)+" "+str(i)+" ")
    return d,f







n=len(sys.argv)

if n>1:
    if sys.argv[1]=="-help":

        print("Suffix Array Project - Help section\nTo start using the program, simply execute it without any argument, the program will ask for the String to calculate the Suffix Array.\nAfter printing it, you will be asked for a pattern to perform Simple Search")
        
    
    else: print("Unknow argument, use command help to see instructions.\n")

else:
    strin=str(input("Enter string to create SA:"))
    #SA=sorted([strin[i:] for i in range(len(strin))]) #Li = T[SA[i]..n − 1] 
    #this is correct but it is a waste of time since we are already going to obtain the indexes on our suffix array

    SAindex=SuffixArray(strin)
    SA=[]
    for i in range(len(SAindex)):
        SA.append(strin[SAindex[i]:])
    SAindex.insert(0,len(strin))
    SA=[]
    for i in range(len(SAindex)):
        if i==0:
            SA.append(" ")
        else:
            SA.append(strin[SAindex[i]])

    

    print("SA: ")
    print(SAindex)
    word=input("Enter pattern to perform SA-Simple Search.\n")
    res, resy=SimpleSearch(SA,word)
    if res==-1: 
        resReal=len(SAindex)
    resReal=SAindex[res]
    if resy != None: 
        print(resy)
        resyReal=SAindex[resy]
        print("Solution: (" + str(resReal)+","+ str(resyReal)+")" )
    else:print("Solution: (" + str(resReal) +")" )

    



    

